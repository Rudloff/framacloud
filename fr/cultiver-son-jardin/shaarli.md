# Installation de Shaarli

[Shaarli][1] est un logiciel libre qui permet, entre autre, de conserver
en ligne ses marque-pages et de les partager.
Il est initialement développé par [Sébastien Sauvage][2] et repris par
la communauté Shaarli.

![](images/shaarli/shaarli.png)

Nous proposons ce logiciel comme service en ligne sur [MyFrama][3].
Il est extrêmement simple à installer sur un serveur (même mutualisé)
et peu gourmand en ressources car il ne requiert aucune base de données.

![](images/shaarli/myframa.jpg)

Voici un tutoriel pour vous aider à l’installer sur votre serveur.

<div class="alert alert-info">
  Dans la suite de ce tutoriel, nous supposerons que vous avez déjà fait
  pointer votre nom de domaine sur votre serveur auprès de votre
  <a href="https://fr.wikipedia.org/wiki/Bureau_d%27enregistrement">registraire</a>
  et que vous disposez d’un serveur web et de PHP en version supérieure à 5.2.6.
</div>

## Installation

### 1 - Planter

![](images/icons/bonzai.png)

Téléchargez les fichiers de la dernière version sur le [dépôt Github officiel][4]
(bouton vert « Clone or Download » puis « Download ZIP » à droite ou
bien en ligne de commande `git clone https://github.com/shaarli/Shaarli.git`).

Décompressez l’archive et copiez le tout sur votre serveur web.
Pour utiliser le logiciel, il suffit de vous rendre sur
`http://votre-site.org/Shaarli-master/` et de suivre la procédure
d’installation (vous pouvez évidemment renommer le dossier) :

![](images/shaarli/shaarli-install.png)

1.  choisissez votre nom d’utilisateur
2.  votre mot de passe
3.  votre fuseau horaire
4.  le titre du site

<div class="alert alert-warning">
  Les dossiers <code>Shaarli-master/cache</code>, <code>Shaarli-master/data</code>,
  <code>Shaarli-master/pagecache</code> et <code>Shaarli-master/tmp</code>
  et doivent être accessibles en écriture par Shaarli.
  Il est aussi important que le serveur soit configuré pour autoriser les sessions.
</div>

![](images/shaarli/shaarli-home.png)

### 2 - Tailler et désherber

![](images/icons/tailler.png)

Par défaut, le logiciel est uniquement en anglais. Pour remédier à ça,
nous avons développé un plugin pour l’internationaliser ainsi qu’un template adapté.

Pour les installer, [téléchargez les fichiers du dépôt git de MyFrama][5].
Copiez les dossiers `/shaarli/plugin/i18n` et `/shaarli/tpl/frama` de
MyFrama à l’identique sur votre instance de Shaarli.

Vous pouvez aussi copier le plugin de gestion avancée des tags
`/shaarli/plugin/tags_advanced` qui permet d'afficher ses tags préférés
sur la page d’accueil et de les attribuer automatiquement à vos liens
grâce aux filtres personnalisés.
Le fichier `/shaarli/plugin/tags_advanced/example.php` doit être renommé,
accessible en écriture et déplacé dans `/data/tags_advanced.php`.

Éditez ensuite le fichier `data/config.php` et remplacez les lignes

    $GLOBALS['config']['RAINTPL_TPL'] = 'tpl/';

et

    $GLOBALS['config']['ENABLED_PLUGINS'] = array (
      0 => 'qrcode',
    );

par

    $GLOBALS['config']['RAINTPL_TPL'] = 'tpl/frama/';

et

    $GLOBALS['config']['ENABLED_PLUGINS'] = array (
      0 => 'i18n',
      1 => 'tags_advanced',
      2 => 'qrcode',
    );

Les fichiers de langue de Shaarli et des plugins compatibles avec `i18n`
se trouvent dans les dossiers `/locale/` respectifs.

![](images/shaarli/shaarli-myframa.png)

### 3 - Bouturer

![](images/icons/semer-pot.png)

Pour aller plus loin, vous pourriez avoir envie de proposer des instances
multiples de Shaarli sur le même modèle que MyFrama.
Pour cela, il suffit de cloner à l’identique [notre dépôt][6] et créer
un dossier `/u/` qui contiendra les dossiers de chaque utilisateur.

Chaque compte sera automatiquement créé depuis la page d’accueil.

Les principaux dossiers de Shaarli sont des liens symboliques relatifs
qui pointent vers `/shaarli/`.
Seuls les dossiers `cache`, `data`, `pagecache`, `tmp` appartiennent
réellement à l’utilisateur.
Ils n’existent volontairement pas dans le dossier `/shaarli/` pour
empêcher la procédure classique d’installation d’être suivie.

Sur MyFrama nous avons ajouté 3 plugins supplémentaires :

1.  `framanav` qui sert à ajouter la barre de navigation de Framasoft
2.  `recovery` qui permet aux utilisateurs de restaurer eux-même leur
    compte en case de perte de mot de passe puisqu’ils ne disposent
    pas d’un accès direct au serveur
3.  `myframa` qui, d’un côté, assure la glu entre la page d’accueil,
    le Shaarli de l’utilisateur et la barre de navigation de Framasoft
    et qui, de l'autre, permet de verrouiller certains paramètres que
    l’utilisateur ne doit pas modifier au risque de casser son instance
    (par exemple, s’il désactive le plugin `i18n`).

Le plugin `myframa` contient un fichier `datastore.php` modèle avec les
liens proposés en exemple pour tout nouveau compte créé.

La configuration par défaut des utilisateurs (et notamment la liste des
plugins actifs), est créée à partir du fichier `index.php` à la racine
qu’il vous faudra probablement adapter.

![](images/shaarli/tree-charlot.jpg)

 [1]: https://github.com/shaarli/Shaarli/
 [2]: http://sebsauvage.net/
 [3]: https://my.framasoft.org
 [4]: https://github.com/shaarli/Shaarli.git
 [5]: https://framagit.org/framasoft/myframa/repository/archive.zip?ref=master
 [6]: https://framagit.org/framasoft/myframa/