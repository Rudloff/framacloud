# Installation de Jitsi Meet

[Jitsi Meet][1] est un logiciel libre distribué sous licence [Apache 2][2]
qui a pour objectif de fournir une solution de visio-conférence accessible
depuis un navigateur web.
Il s’agit de la solution logicielle qui propulse le service en ligne [Framatalk][3].

<div class="alert alert-info">
  Ce guide est prévu pour Debian Stable. À l'heure où nous écrivons ces
  mots, il s'agit de Debian 8.6 Jessie. Nous partons du principe que
  vous venez d'installer le système, que celui-ci est à jour et que
  vous avez déjà installé le serveur web <a href="http://nginx.org/">Nginx</a>.
</div>

## 1 - Nourrir la terre

![](images/icons/preparer.png)

Le dépôt fourni par Jitsi utilisant le chiffrement, nous allons devoir installer `apt-transport-https` :

    apt-get install apt-transport-https

Afin d'installer Jitsi il est nécessaire d'avoir les dépots jessie-backports pour installer Java Runtime Environment (étape non nécessaire pour Debian stretch)

    echo 'deb http://ftp.fr.debian.org/debian/ jessie-backports main' >> /etc/apt/sources.list

Ajoutons aussi l'adresse du dépôt jitsi :

    echo 'deb https://download.jitsi.org stable/' >> /etc/apt/sources.list.d/jitsi-stable.list

Et importons les clés des *packageurs* de Jitsi dans le trousseau de notre système de paquet :

    wget -qO -  https://download.jitsi.org/jitsi-key.gpg.key | apt-key add -

<div class="alert alert-info">
  Un petit mot sur les clés : les paquets sont signés avec GPG par ceux
  qui les créent.
  La vérification de la signature d'un paquet permet de s'assurer de son
  intégrité (de vérifier qu'il n'a pas été trafiqué par une personne malveillante).
  Mais pour vérifier la signature, il faut s'assurer qu'elle a été créée
  par une clé dont nous connaissons la légitimité (nous vérifions là
  l'authenticité du paquet).<br/>
  D'où le <code>wget… | apt-key add -</code> pour intégrer les clés des
  <em>packageurs</em> de Jitsi dans le trousseau de clés considérées
  comme fiables par notre système de paquet
</div>

Mettons à jour notre liste de paquets disponibles :

    apt-get update

Installation de Java Runtime Environment (/!\ enlever ` -t jessie-backports` si la distribution est Debian stretch)

    apt-get install -t jessie-backports openjdk-8-jre-headless ca-certificates-java


## 2 - Semer

![](images/icons/semer.png)

L'installation en elle-même est confondante de simplicité :

    apt-get install jitsi-meet

Outre des composants propres à Jitsi Meet (`jicofo` et `jitsi-videobridge`),
ceci installera aussi le serveur XMPP Prosody.
En effet, Jitsi Meet utilise le protocole XMPP et nécessite donc un tel serveur.

L'installeur va vous demander le nom DNS de votre instance.
Choisissons `meet.example.org` pour notre exemple :

![](images/jitsi-meet/Screenshot_20160922_130945.png)

Si vous ne disposez pas d'un nom de domaine, vous pouvez mettre l'IP du
serveur (encore faut-il qu'elle ne change pas).

Puis si on veut utiliser un certificat auto-signé ou si on en a déjà un
à lui fournir :

![](images/jitsi-meet/Screenshot_20160922_131015.png)

La génération d'un certificat n'étant pas le sujet de ce tutoriel, on
choisira le certificat auto-signé.
Retenez simplement que si vous souhaitez changer de certificat,
il faudra modifier le fichier de configuration
`/etc/prosody/conf.avail/meet.example.org.cfg.lua` et relancer prosody
(`service prosody restart`).

Pour la configuration, cela se passe dans le fichier
`/etc/jitsi/meet/meet.example.org-config.js`, mais il y a peu à modifier :

    // Pour choisir la langue par défaut
    defaultLanguage: "fr",
    // Éviter de faire des requêtes à d'autres sites (google analytics, gravatar et callstats.io)
    disableThirdPartyRequests: true,

## 3 - Arroser

![](images/icons/arroser.png)

Pour la configuration de Nginx… Il n'y a rien à faire !

Un fichier `/etc/nginx/sites-available/meet.example.org.conf` et un
lien dans `/etc/nginx/sites-enabled` ont été créés à l'installation de
`jitsi-meet` :-)

Si vous changez de certificat, vous devrez aussi modifier
`/etc/nginx/sites-available/meet.example.org.conf` pour qu'il utilise
votre certificat.

## 4 - Regarder pousser

![](images/icons/pailler.png)

Vous noterez que la page d'accueil de votre instance de Jitsi Meet n'a
vraiment rien à voir avec celle que nous avons sur <https://framatalk.org>.
Pour pouvoir avoir une page d'accueil personnalisée, nous effectuons
tout simplement une petite redirection au niveau de Nginx
(dans `/etc/nginx/sites-available/meet.example.org.conf` donc) :

    rewrite ^\/$ https://meet.example.org/accueil/ break;
    location ~ /accueil/ {
         # Nous avons mis notre page d'accueil personnalisée dans /var/www/accueil/
         alias /var/www/accueil/;
    }

Attention ! Notre `location ~ /accueil/ {…}` doit impérativement être
placé avant le bloc `location ~ ^/([a-zA-Z0-9=\?]+)$ {…}` pour être
pris en compte !

Nous avons aussi modifié, fourbes que nous sommes, un fichier de
configuration caché. Il s'agit de la configuration de l'interface Pour cela :

    mkdir /var/www/jitsi-custom
    cp /usr/share/jitsi-meet/interface_config.js /var/www/jitsi-custom

Ceci nous permet d'éviter que nos modifications soient écrasées lors
d'une mise à jour. Éditons `/var/www/jitsi-custom/interface_config.js` :

    // Un peu de traduction des termes
    DEFAULT_REMOTE_DISPLAY_NAME: "Autre participant·e",
    DEFAULT_LOCAL_DISPLAY_NAME: "moi",
    // Un peu de personnalisation
    JITSI_WATERMARK_LINK: "https://framatalk.org",
    APP_NAME: "Framatalk",
    // Ici, vous pouvez modifier ce qu'il y a dans la barre d'outils
    TOOLBAR_BUTTONS: ['microphone', 'camera', etc],

Pour que ce soit notre fichier modifié qui soit utilisé et non celui de
Jitsi, il faut ajouter dans la configuration Nginx :

    location = /interface_config.js {
        alias /var/www/jitsi-custom/interface_config.js;
    }

Notez que vous pouvez faire ça pour d'autres fichiers :-)

    location = /images/watermark.png {
        alias /var/www/jitsi-custom/mon_watermark.png;
    }

Et comme chaque fois que l'on modifie une configuration Nginx,
pour la prendre en compte :

    nginx -t && nginx -s reload

(`nginx -t` permet de vérifier que la configuration de Nginx est valide
avant de le relancer)

<p class="text-center">
  <span class="fa fa-leaf" aria-hidden="true" style="font-size:200px; color:#333"></span>
</p>

 [1]: http://meet.jit.si/
 [2]: https://github.com/jitsi/jitsi-meet/blob/master/LICENSE
 [3]: https://framatalk.org
