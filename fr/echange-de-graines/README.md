# Échange de graines

<div class="alert alert-info" style="margin-top:25px;">
Cette rubrique contient quelques astuces et tutoriels concernant des logiciels
que Framasoft utilise au quotidien dans la gestion de son infrastructure.<br>
Ils s’adressent surtout à un public d’administrateur⋅trice⋅s systèmes.
</div>

## Listes des articles

* [Résoudre un temps de login trop long](resoudre-un-temps-de-login-trop-long.html)
* [Trouver quel processus utilise un port](trouver-quel-processus-utilise-un-port.html)
* [Débloquer un RAID coincé en resync=PENDING](debloquer-un-raid-coince-en-resyncpending.html)
* [Ajouter une autorité de certification à Debian](ajouter-une-autorite-de-certification-a-debian.html)
* [Astuces sur RequestTracker](astuces-sur-requesttracker.html)
* [Supprimer tous les mails de la file d'attente postfix pour un domaine particulier](supprimer-tous-les-mails-de-la-file-dattente-de-postfix-pour-un-domaine-particulier.html)
* [Personnaliser son instance Gitlab : le script de Framasoft](personnaliser-son-instance-gitlab-le-script-de-framasoft.html)
* [Cross-compilation de RethinkDB pour processeur ARM](cross-compilation-de-rethinkdb-pour-processeur-arm.html)
