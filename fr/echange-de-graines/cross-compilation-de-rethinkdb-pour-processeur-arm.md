# Cross-compilation de RethinkDB pour processeur ARM

Nous supposons que vous avez déjà installé [Docker](https://www.docker.com/), sinon, [faites-le](https://store.docker.com/search?type=edition&offering=community).

Téléchargez notre Dockerfile et lancez le conteneur :

    mkdir rethinkdb-x-compil
    cd rethinkdb-x-compil
    wget https://framacloud.org/fr/echange-de-graines/cross-compilation-de-rethinkdb-pour-processeur-arm.Dockerfile -O Dockerfile
    sudo docker build .

La compilation mettra du temps, mais quand elle sera finie, vous aurez un paquet Debian nommé `rethinkdb_2.3.6+fallback~0_armhf.deb` qu'il vous faudra récupérer du conteneur.

    docker images | grep none
    # Notez le contenu de la colonne IMAGE ID
    docker run -t le_image_id

Dans un autre terminal :

    docker ps | grep le_image_id
    # Notez le contenu de la colonne CONTAINER ID
    docker cp le_container_id:/home/jedi/rethinkdb-2.3.6/build/packages/rethinkdb_2.3.6+fallback~0_armhf.deb  rethinkdb_2.3.6+fallback~0_armhf.deb
    docker stop le_container_id

Vous pourrez alors envoyer le paquet Debian récupéré sur votre machine ARM et retourner à la page d'[installation de Turtl](../cultiver-son-jardin/turtl.html).
